@store
Feature: Basic API feature

  Scenario Outline: STORE Place an order for a pet
    When I request to create a user "<endpoint>" object json_store
    Then I should get <expectedStatusCode> status code
    Examples:
      | expectedStatusCode | endpoint     |
      | 200                | /store/order |

  Scenario Outline: STORE Find purchase order by ID
    When I request to get a user "<endpoint>" by ID "<id>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | id | key | value | expectedStatusCode | endpoint       |
      | 5  | id  | 5     | 200                | /store/order/5 |

  Scenario Outline: STORE Delete a pet
    When I request to get a user "<endpoint>" by ID "<id>"
    Then I should get <expectedStatusCode> status code
    Examples:
      | id | expectedStatusCode | endpoint       |
      | 5  | 200                | /store/order/5 |
