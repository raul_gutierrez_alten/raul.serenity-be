@user
Feature: Basic API feature

  Scenario Outline: Create user
    When I request to create a user "<endpoint>" object json_users
    Then I should get <expectedStatusCode> status code
    Examples:
      | expectedStatusCode | endpoint |
      | 200                | /user    |


  Scenario Outline: Creates list of users with given input array
    When I request to create a user "<endpoint>" object json_users_array
    Then I should get <expectedStatusCode> status code
    Examples:
      | expectedStatusCode | endpoint              |
      | 200                | /user/createWithArray |


  Scenario Outline: Creates list of users with given input array
    When I request to create a user "<endpoint>" object json_users_array
    Then I should get <expectedStatusCode> status code
    Examples:
      | expectedStatusCode | endpoint             |
      | 200                | /user/createWithList |

  Scenario Outline: Logs user into the system
    When I request to get a "<user>" by user and password "<pwd>" "<endpoint>"
    Then I should get <expectedStatusCode> status code
    Examples:
      | expectedStatusCode | user | pwd    | endpoint    |
      | 200                | quim | proces | /user/login |


  Scenario Outline: Logs out current logged in user session
    When I request to get a "<user>" by user and password "<pwd>" "<endpoint>"
    Then I should get <expectedStatusCode> status code

    Examples:
      | expectedStatusCode | user | pwd    | endpoint     |
      | 200                | quim | proces | /user/logout |


  Scenario Outline: Get user by user name
    When I request to get a user "<endpoint>" by ID "<username>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"

    Examples:
      | username | expectedStatusCode | key      | value  | endpoint   |
      | quim     | 200                | username | quim   | /user/quim |
      | quim     | 200                | password | proces | /user/quim |


  Scenario Outline: Updated user
    When I request to get a user "<endpoint>" by ID "<username>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"

    Examples:
      | username | expectedStatusCode | key      | value | endpoint   |
      | quim     | 200                | username | quim  | /user/quim |


  @user
  Scenario Outline: Delete user
    When I request to get a user "<endpoint>" by ID "<username>"
    Then I should get <expectedStatusCode> status code
    Examples:
      | username | expectedStatusCode | endpoint   |
      | perico   | 200                | /user/quim |
