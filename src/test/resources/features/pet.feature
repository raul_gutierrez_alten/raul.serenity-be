@pet
Feature: Basic API feature

  Scenario Outline: Add a new pet with XML
    When I request to create a pet with XML
    Then I should get <expectedStatusCode> status code
    Examples:
      | expectedStatusCode |
      | 200                |

  Scenario Outline: Add a new pet to the store
    When I request to create a user "<endpoint>" object json_pet
    Then I should get <expectedStatusCode> status code
    Examples:
      | expectedStatusCode | endpoint |
      | 200                | /pet     |

  Scenario Outline: Update an existing pet
    When I request to create a user "<endpoint>" object json_pet
    Then I should get <expectedStatusCode> status code
    Examples:
      | expectedStatusCode | endpoint |
      | 200                | /pet     |

  Scenario Outline: Finds Pet by ID
    When I request to get a user "<endpoint>" by ID "<id>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | id | key | value | expectedStatusCode | endpoint |
      | 10 | id  | 10    | 200                | /pet/10  |

  Scenario Outline: Updates a pet in the store with form data
    When I request to get a user "<endpoint>" by ID "<id>"
    Then I should get <expectedStatusCode> status code
    And The value for the "<key>" after get operation should be "<value>"
    Examples:
      | id | key | value | expectedStatusCode | endpoint |
      | 10 | id  | 10    | 200                | /pet/10  |

  Scenario Outline: Delete a pet
    When I request to get a user "<endpoint>" by ID "<id>"
    Then I should get <expectedStatusCode> status code
    Examples:
      | id | expectedStatusCode | endpoint |
      | 10 | 200                | /pet/10  |
