node {
    try {
        currentBuild.result = 'SUCCESS'

        stage('Clone') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    deleteDir()
                    sh 'git clone https://raul_gutierrez_alten@bitbucket.org/raul_gutierrez_alten/raul.serenity-be.git'
                    echo '${WORKSPACE}'
                } else {
                    deleteDir()
                    bat 'git clone https://raul_gutierrez_alten@bitbucket.org/raul_gutierrez_alten/raul.serenity-be.git'
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Build') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir('raul.serenity-be') {
                        sh './gradlew clean'
                    }
                } else {
                    dir('raul.serenity-be') {
                        bat 'gradle clean'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Test') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir('\\raul.serenity-be') {
                        sh './gradlew clean'
                    }
                } else {
                    dir('\\raul.serenity-be') {
                        bat 'gradle test aggregate'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Serenity Report') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir('/raul.serenity-be') {
                        publishHTML([
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : false,
                                reportDir            : '/target/site/serenity',
                                reportFiles          : 'index.html',
                                reportName           : 'Serenity',
                                reportTitles         : ''
                        ])
                    }
                } else {
                    dir('\\raul.serenity-be') {
                        publishHTML([
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : false,
                                reportDir            : '\\target\\site\\serenity',
                                reportFiles          : 'index.html',
                                reportName           : 'Serenity',
                                reportTitles         : ''
                        ])
                    }
                }

            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Cucumber Report') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir('/var/jenkins_home/workspace/serenity-be/raul.serenity-be') {
                        cucumber fileIncludePattern: 'cucumber-report.json', jsonReportDirectory: '/target', sortingMethod: 'ALPHABETICAL'
                    }
                } else {
                    dir('\\raul.serenity-be') {
                        cucumber fileIncludePattern: 'cucumber-report.json', jsonReportDirectory: '\\target', sortingMethod: 'ALPHABETICAL'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Send email') {
            emailext body: 'Test done', subject: 'Jenkins test', to: 'raul.gutierrez@alten.es'
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

}