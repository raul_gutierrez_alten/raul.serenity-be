package com.adidas.serenitySteps;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import io.restassured.response.Response;

import static net.serenitybdd.rest.SerenityRest.rest;
import com.adidas.config.ServicesConfiguration;
import com.adidas.support.ServicesSupport;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

import net.thucydides.core.annotations.Step;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

public class ExampleSteps{

    private ServicesSupport servicesSupport = new ServicesSupport();
    private RequestSpecification specJSON = rest().baseUri(ServicesConfiguration.URI).contentType(ContentType.JSON).when();
    private RequestSpecification specXML = rest().baseUri(ServicesConfiguration.URI).contentType(ContentType.XML).when();

    public String getJSON_XMLPath(String json){
        String path = "";
        switch(json){
            case "json_pet":
                path="/request/create_user1.json";
                break;
            case "xml_pet":
                path="/request/create_pet.xml";
                break;
            case "json_store":
                path="/request/create_user2.json";
                break;
            case "json_users":
                path="/request/create_user3.json";
                break;
            default:
                path="/request/create_user3array.json";
                break;
        }
        return path;
    }

    /**
     * Performs a GET operation with an ID provided by parameter from the scenario
     * @param id The ID of a user
     * */
    @Step
    public void getUserById(String id, String endpoint) {

        String endpointStatus = endpoint + "?status=" + id;
        Response response = servicesSupport.executeRequest(specJSON,"GET", endpointStatus);
        Serenity.setSessionVariable("response").to(response);
    }

    @Step
    public void getUserByUserPwd(String user, String pwd, String endpoint) {

        String endpointUserPwd = endpoint + "?username=" + user + "&password=" + pwd;
        Response response = servicesSupport.executeRequest(specJSON,"GET", endpointUserPwd);
        Serenity.setSessionVariable("response").to(response);
    }

    @Step
    public void postUserById(String id, String endpoint){
        String endpointPost = endpoint + "/" + id;
        Response response = servicesSupport.executeRequest(specJSON,"POST", endpointPost);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Performs a POST operation that will create a new user
     * */
    @Step
    public void createUserArray(String endpoint, String jsonPath){

        try {
            InputStream is = this.getClass().getResourceAsStream(getJSON_XMLPath(jsonPath));
            JSONArray body = servicesSupport.jsonInputStreamToJsonArray(is);
            specJSON = specJSON.body(body.toList());
            Response response = servicesSupport.executeRequest(specJSON,"POST", endpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    @Step
    public void createUserObject(String endpoint, String jsonPath){

        try {
            InputStream is = this.getClass().getResourceAsStream(getJSON_XMLPath(jsonPath));
            JSONObject body = servicesSupport.jsonInputStreamToJsonObject(is);
            specJSON = specJSON.body(body.toMap());
            Response response = servicesSupport.executeRequest(specJSON,"POST", endpoint);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    @Step
    public void createPetXML(){

        try {
            InputStream is = this.getClass().getResourceAsStream("/request/pet.xml");
            specXML = specXML.body(readXML("pet.xml"));
            Response response = servicesSupport.executeRequest(specXML,"POST", "/pet");
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    public String readXML(String nameFile) {
        String operationXml = "";
        try {
            InputStream inputStream = this.getClass().getResourceAsStream("/request/".concat(nameFile));

            InputStreamReader is = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader buffer = new BufferedReader(is);
            operationXml = buffer.lines().collect(Collectors.joining("\n"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return operationXml;
    }

    /**
     * Performs a PUT operation with an ID from the scenario as a parameter
     * @param id The ID of a user
     * */
    @Step
    public void updateUserById(String id, String endpoint) {

        try {
            InputStream is = this.getClass().getResourceAsStream("/request/update_user.json");
            String endpointId = endpoint + "/" + id;
            JSONArray body = servicesSupport.jsonInputStreamToJsonArray(is);
            specJSON = specJSON.body(body.toList());
            Response response = servicesSupport.executeRequest(specJSON,"PUT", endpointId);
            Serenity.setSessionVariable("response").to(response);
        }
        catch(Exception e){
            e.getMessage();
        }
    }

    /**
     * Performs a DELETE operation with an ID provided by parameter from the scenario
     * @param id The ID of a user
     * */
    @Step
    public void deleteUserById(String id, String endpoint) {

        String endpointDel = endpoint + "/" + id;
        Response response = servicesSupport.executeRequest(specJSON,"DELETE", endpointDel);
        Serenity.setSessionVariable("response").to(response);
    }

    /**
     * Method to verify an status code received from the scenario
     * @param expectedStatusCode Expected status code in the response
     * */
    @Step
    public void verifyStatusCode(int expectedStatusCode){
        Response res = Serenity.sessionVariableCalled("response");
        Assert.assertEquals("status code doesn't match", expectedStatusCode, res.getStatusCode());
    }

    /**
     * Method to verify an status code received from the scenario
     * @param res Response object from a previous operation
     * @param operation The operation that was done in a previous step received from Cucumber
     * @param key Attribute name received from the scenario as a parameter
     * @param expectedValue Expected value of the attribute received from the scenario as a parameter
     * */
    @Step
    public void verifyValueFromKey(String value, Response res, String operation, String key, String expectedValue) {
        List<Integer> data;
        String currentValue = "";

        switch (operation.toLowerCase()) {
            case "get":
                if(value.equals("value")){
                    currentValue = ""+res.getBody().jsonPath().getString(key);
                }else{
                    data = res.getBody().jsonPath().getList(key);
                    for(int d:data){
                        if((Integer.toString(d)).equals(expectedValue)){
                            currentValue = d+"";
                            break;
                        }
                    }
                }
                break;
            case "post":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            case "update":
                currentValue = res.getBody().jsonPath().getString(key);
                break;
            case "delete":
                currentValue = res.getBody().jsonPath().getString("data." + key);
                break;
            default:
                break;
        }

        Assert.assertEquals("Value for " + key + " doesn't match", expectedValue, currentValue);
    }
}