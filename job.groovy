node {
    def git = 'git clone --single-branch -b testing_services https://raul_gutierrez_alten@bitbucket.org/raul_gutierrez_alten/raul.serenity-be.git'
    def projectDirectory = "${WORKSPACE}"+'/raul.serenity-be'
    try {
        currentBuild.result = 'SUCCESS'

        stage('Clone') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    deleteDir()
                    sh git
                } else {
                    deleteDir()
                    bat git
                }
            }
        }

        stage('Build') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir(projectDirectory) {
                        sh 'chmod 777 ./gradlew'
                        sh './gradlew clean'
                    }
                } else {
                    dir('raul.serenity-be') {
                        bat 'gradle clean'
                    }
                }
            }
        }

    try {
        currentBuild.result = 'SUCCESS'

        stage('Test') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir(projectDirectory) {
                        sh './gradlew test aggregate'
                    }
                } else {
                    dir('\\raul.serenity-be') {
                        bat 'gradle test aggregate'
                    }
                }
            }
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

        stage('Serenity Report') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir(projectDirectory) {
                        echo 'LLEGA'
                        echo "${WORKSPACE}"+'/raul.serenity-be/target/site/serenity'
                        publishHTML([
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : false,
                                reportDir            : "${WORKSPACE}"+'/raul.serenity-be/target/site/serenity',
                                reportFiles          : 'index.html',
                                reportName           : 'Serenity',
                                reportTitles         : ''
                        ])
                    }
                } else {
                    dir('\\raul.serenity-be') {
                        publishHTML([
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : false,
                                reportDir            : '\\target\\site\\serenity',
                                reportFiles          : 'index.html',
                                reportName           : 'Serenity',
                                reportTitles         : ''
                        ])
                    }
                }

            }
        }

        stage('Cucumber Report') {
            timeout(time: 2, unit: 'MINUTES') {
                if (isUnix()) {
                    dir(projectDirectory) {
                        cucumber fileIncludePattern: 'cucumber-report.json', jsonReportDirectory: "${WORKSPACE}"+'/raul.serenity-be/target', sortingMethod: 'ALPHABETICAL'
                    }
                } else {
                    dir('\\raul.serenity-be') {
                        cucumber fileIncludePattern: 'cucumber-report.json', jsonReportDirectory: '\\target', sortingMethod: 'ALPHABETICAL'
                    }
                }
            }
        }



        stage('Send email') {
            emailext body: 'Test done', subject: 'Jenkins test', to: 'raul.gutierrez@alten.es'
        }
    } catch (err) {
        echo "Caught: ${err}"
        currentBuild.result = 'FAILURE'
    }

}